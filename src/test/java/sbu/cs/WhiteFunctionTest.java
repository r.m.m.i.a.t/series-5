package sbu.cs;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WhiteFunctionTest {

    private static WhiteFunction A ;
    String input1 = "raziye" , input2 = "pegah" ;
    @BeforeAll
    static void setUp() {
        A = new WhiteFunction() ;
    }

    @Test
    void one() {
        assertEquals( A.one( input1 , input2 ) , "rpaezgiayhe");
    }

    @Test
    void two() {
        assertEquals( A.two( input1 , input2 ) , "raziyehagep");
    }

    @Test
    void three() {
        assertEquals( A.three( input1 , input2 ) , "rhaazgieype");
    }

    @Test
    void four() {
        assertEquals( A.four( input1 , input2 ) , "raziye");
    }

    @Test
    void five() {
        assertEquals( A.five( input1 , input2 ) , "gefife");
    }
}