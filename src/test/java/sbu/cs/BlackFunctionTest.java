package sbu.cs;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BlackFunctionTest {

    private static BlackFunction A ;
    String input = "raziye" ;
    @BeforeAll
    static void setUp() {
        A = new BlackFunction() ;
    }

    @Test
    void one() {
        assertEquals( A.one(input) , "eyizar");
    }

    @Test
    void two() {
        assertEquals( A.two(input) , "rraazziiyyee");
    }

    @Test
    void three() {
        assertEquals( A.three(input)  , "raziyeraziye");
    }

    @Test
    void four() {
        assertEquals( A.four(input) , "aziyer");
    }

    @Test
    void five() {
        assertEquals( A.five(input) , "izarbv");
    }
}