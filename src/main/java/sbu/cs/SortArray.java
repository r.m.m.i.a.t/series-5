package sbu.cs;
import java.util.*;


public class SortArray {



    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

       boolean[]  check = new boolean[size];
       int[] ans = new int[size] ;
       Arrays.fill(check,false);
       for ( int i = 0 ; i < size ; i++ )
       {
           int minVal = - 1 , index = -1 ;
           for ( int j = 0 ; j < size ; j++ )
           {
               if (!check[j] && index == -1)
               {
                   minVal = arr[j];
                   index = j;
               }

               if (!check[j] && arr[j] < minVal)
               {
                   minVal = arr[j];
                   index = j;
               }

           }
           ans[i] = minVal ;
           check[index] = true ;
       }
       return ans ;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {

        boolean[]  check = new boolean[size] ;
        int[] ans = new int[size] ;
        Arrays.fill(check,false);
        for ( int i = 0 ; i < size ; i++ )
            for ( int j = 0 ; j < i ; j++ )
            {
                if ( arr[j] >= arr[i] )
                {
                    int val = arr[i] ;
                    for ( int k = i-1 ; k >= j ; k-- )
                    {
                        arr[k+1] = arr[k] ;
                    }
                    arr[j] = val ;
                    break;
                }
            }
        return arr ;

    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if ( size == 1 )
            return arr ;

        int n1 = size/2 , n2 = (size+1)/2 ;

        int[] arr1 = new int[n1] ;
        int[] arr2 = new int[n2] ;

        for ( int i = 0 ; i < n1 ; i++ )
            arr1[i] = arr[i] ;
        for ( int i = 0 ; i < n2 ; i++ )
            arr2[i] = arr[n1+i] ;


        arr1 = mergeSort( arr1 , n1 ) ;
        arr2 = mergeSort( arr2 , n2 ) ;

        int[] ans = new int[size] ;

        int ind1 = 0 , ind2 = 0 , index = 0 ;
        while ( ind1 < n1 && ind2 < n2 )
        {
            if ( arr1[ind1] <= arr2[ind2] )
            {
                ans[index] = arr1[ind1] ;
                index++ ;
                ind1++ ;
            }
            else
            {
                ans[index] = arr2[ind2] ;
                index++ ;
                ind2++ ;
            }

        }
        if ( ind1 < n1 )
            for ( int i = ind1 ; i < n1 ; i++ , index++ )
                ans[index] = arr1[i] ;
        else
            for ( int i = ind2 ; i < n2 ; i++ , index++ )
                ans[index] = arr2[i] ;

        return  ans ;

    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int beg = 0 , end = arr.length;
        while (end - beg > 1 )
        {
            int mid = ( beg + end ) / 2 ;
            if ( value < arr[mid] )
            {
                end = mid ;
            }
            else
            {
                beg = mid ;
            }
        }
        if ( arr[beg] == value )
            return beg ;
        else
            return -1 ;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int bs ( int[] arr , int beg , int end , int value )
    {
        if ( end - beg == 1 )
        {
            if ( arr[beg] == value )
                return beg ;
            return -1 ;
        }
        int mid = ( beg + end ) / 2 ;
        if ( arr[mid] <= value )
            return bs ( arr , mid , end , value ) ;
        return bs ( arr , beg , mid , value ) ;
    }
    public int binarySearchRecursive(int[] arr, int value) {
        return bs ( arr , 0 , arr.length , value ) ;
    }
}
