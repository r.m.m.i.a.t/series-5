package sbu.cs;

public class YellowSquare extends BasicSquare {

    YellowSquare ( int number )
    {
        this.number = number ;
    }

    String input , output ;
    BlackFunction blackFunction = new BlackFunction() ;

    public void setInput ( String input )
    {
        this.input = input ;
    }

    public String getOutput ()
    {

        switch (number)
        {
            case 1 :
                output = blackFunction.one(input) ;
            case 2 :
                output = blackFunction.two(input) ;
            case 3 :
                output = blackFunction.three(input) ;
            case 4 :
                output = blackFunction.four(input) ;
            case 5 :
                output = blackFunction.five(input) ;
        }

        return output ;
    }
}
