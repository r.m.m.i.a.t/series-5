package sbu.cs;

public class App {


    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */

    int n ;
    int[][] arr ,  colorArr ;
    String input ;
    BasicSquare[][] map ;
    public String[][][] inputs ;

    public void preProcess ()
    {


        /// Yellow
            colorArr[0][n-1] = 0 ;
            colorArr[n-1][0] = 0 ;

        /// Green
            for ( int i = 0 ; i < n-1 ; i++ )
            {
                colorArr[0][i] = 1 ;
                colorArr[i][0] = 1;
            }

        /// Pink
            for ( int i = 1 ; i < n ; i++ ) {
                colorArr[i][n - 1] = 2;
                colorArr[n - 1][i] = 2;

            }

        /// Blue
           for ( int i = 1 ; i < n-1 ; i++ )
                for ( int j = 1 ; j < n-1 ; j++)
                    colorArr[i][i] = 3;


    }

    public void yellow ( int x , int y )
    {
        YellowSquare square = new YellowSquare(arr[x][y]) ;


        if ( x == 0 )
        {
            square.setInput(inputs[x][y][1]);
            inputs[x + 1][y][0] = square.getOutput();

        }
        else
        {
            square.setInput(inputs[x][y][0]) ;
            inputs[x][y+1][1]  = square.getOutput() ;

        }

//        map[x][y] = square ;
    }

    public void blue ( int x , int y )
    {
        BlueSquare square = new BlueSquare( arr[x][y] ) ;
        square.setUpInput(inputs[x][y][0]);
        square.setLeftInput(inputs[x][y][1]) ;
        inputs[x+1][y][0] = square.getDownOutput();
        inputs[x][y+1][1] = square.getRightOutput() ;

//        map[x][y] = square ;

    }

    public void green ( int x , int y )
    {
        GreenSquare square = new GreenSquare(arr[x][y]) ;
        if ( x == 0 )
            square.setInput(inputs[x][y][1]) ;
        else
            square.setInput(inputs[x][y][0]) ;

        inputs[x][y+1][1] = inputs[x+1][y][0] = square.getOutput() ;

//      map[x][y] = square ;
    }

    public void pink ( int x , int y )
    {
        PinkSquare square = new PinkSquare(arr[x][y]) ;
        square.setUpInput(inputs[x][y][0]) ;
        square.setLeftInput(inputs[x][y][1]) ;
        if ( x == n-1 )
            inputs[x][y+1][1] = square.getOutput() ;
        else
            inputs[x+1][y][0] = square.getOutput() ;

//        map[x][y] = square ;
    }

    public void mainProcess ()
    {
        for ( int i = 0 ; i < n ; i++ )
            for ( int j = 0 ; j < n ; j++ )
            {
                switch (colorArr[i][j])
                {
                    case 0 :
                        yellow(i,j) ;
                    case 1 :
                        green(i,j);
                    case 2 :
                        pink(i,j);
                    case 3 :
                        blue(i,j);
                }
            }
    }


    public String main(int n, int[][] arr, String input) {

        this.n = n ;
        this.arr = arr ;
        this.input = input ;

        map = new BasicSquare[n][n] ;
        colorArr = new int[n][n] ;
        inputs  = new String[n+1][n+1][2] ;
        inputs[0][0][0] = inputs[0][0][1] = input ;

        preProcess();
        mainProcess();

        return inputs[n-1][n][1]  ;

    }
}
