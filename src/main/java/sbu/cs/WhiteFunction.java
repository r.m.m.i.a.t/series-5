package sbu.cs;
import java.lang.StringBuilder ;
public class WhiteFunction {



    public  String one (String a , String b )
    {
        String ans = "" ;
        int min = Math.min(a.length() , b.length() ) ;
        for ( int i = 0 ; i < min ; i++ ) {
            ans += a.charAt(i) ;
            ans += b.charAt(i) ;
        }
        if ( a.length() < b.length() ){
            for ( int i = min ; i < b.length() ; i++ )
                ans += b.charAt(i) ;
        }
        else
        {
            for ( int i = min ; i < a.length() ; i++ )
                ans += a.charAt(i) ;
        }
        return ans ;

    }


    public String two ( String a , String b )
    {

        StringBuilder A = new StringBuilder(a);
        StringBuilder B = new StringBuilder(b) ;
        B.reverse() ;
        A.append(B) ;
        return A.toString() ;
    }


    public String three ( String a , String b )
    {
        StringBuilder B = new StringBuilder(b) ;
        b = B.reverse().toString() ;
        return one(a , b) ;
    }
    public String four ( String a , String b )
    {
        if ( a.length() % 2 == 0 )
            return a ;
        return  b ;
    }
    public String five ( String a , String b )
    {

        String ans = "" ;
        int min = Math.min(a.length() , b.length()) ;
        for ( int i = 0 ; i < min ; i++ ) {
                int x = (int)a.charAt(i) + (int)b.charAt(i) - 'a' - 'a' ;
                x %= 26 ;
                ans += (char)(x+'a') ;

        }

        if ( a.length() < b.length() ){
            for ( int i = min ; i < b.length() ; i++ )
                ans += b.charAt(i) ;
        }
        else
        {
            for ( int i = min ; i < a.length() ; i++ )
                ans += a.charAt(i) ;
        }

        return ans ;
    }


}
