package sbu.cs;

public class BlueSquare extends BasicSquare {


    BlueSquare ( int number )
    {
        this.number = number ;
    }

    String upInput , downOutput , leftInput , rightOutput ;
    BlackFunction blackFunction = new BlackFunction() ;

    public void setLeftInput(String leftInput) {
        this.leftInput = leftInput;
    }

    public void setUpInput(String upInput) {
        this.upInput = upInput;
    }



    public String getRightOutput() {

        switch (number)
        {
            case 1 :
                rightOutput = blackFunction.one(leftInput) ;
            case 2 :
                rightOutput = blackFunction.two(leftInput) ;
            case 3 :
                rightOutput = blackFunction.three(leftInput) ;
            case 4 :
                rightOutput = blackFunction.four(leftInput) ;
            case 5 :
                rightOutput = blackFunction.five(leftInput) ;
        }

        return rightOutput;
    }

    public String getDownOutput() {
        switch (number)
        {
            case 1 :
                downOutput = blackFunction.one(upInput) ;
            case 2 :
                downOutput = blackFunction.two(upInput) ;
            case 3 :
                downOutput = blackFunction.three(upInput) ;
            case 4 :
                downOutput = blackFunction.four(upInput) ;
            case 5 :
                downOutput = blackFunction.five(upInput) ;
        }
        return downOutput;
    }

}
