package sbu.cs;

public class PinkSquare extends BasicSquare {


    PinkSquare ( int number )
    {
        this.number = number ;
    }

    String upInput , output , leftInput ;
    WhiteFunction whiteFunction = new WhiteFunction() ;

    public void setUpInput(String upInput) {
        this.upInput = upInput;
    }

    public void setLeftInput(String leftInput) {
        this.leftInput = leftInput;
    }
    public String getOutput ()
    {
         switch (number)
            {
                case 1 :
                    output = whiteFunction.one( leftInput , upInput ) ;
               case 2 :
                    output = whiteFunction.two( leftInput , upInput ) ;
               case 3 :
                    output = whiteFunction.three( leftInput , upInput ) ;
                case 4 :
                    output = whiteFunction.four( leftInput , upInput ) ;
                case 5 :
                    output = whiteFunction.five( leftInput , upInput ) ;
        }
        return output;
    }

}
